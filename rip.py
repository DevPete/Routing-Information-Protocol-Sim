import sys
import select
import socket
import ConfigParser
import struct
import datetime
import time
import random
import ConfigParser
import os.path
from copy import deepcopy


'''
5th MAY, 2016
'''

class RIP:

    HOST = "127.0.0.1"
    MAX_ROUTES_PER_UPDATE = 24
    DEFAULT_TIMER = 30
    TIMER_OFF_SET = 2


    def __init__(self, routerID, input_ports, outputs, timer):
        """
        routerID: the local Router ID address
        input_ports: the ports to listen on
        outputs: an array including the ports to send data thru, the metrics,
                 and the Router ID (address) for each peer router
        timer
        """
        print("Starting up RIP router...")
        timer = timer
        self.routerID = routerID[0]
        if not timer:
            base_timer = self.DEFAULT_TIMER
        else:
            base_timer = timer[0]

        self.update_timer = base_timer
        self.garbage_collection_timer = base_timer * 4
        self.timeout_timer = base_timer * 6

        self.garbage_timer_on = False
        self.route_change = False

        self.activate_ifaces(input_ports, outputs)
        self._routes = []
        self.localroute = RouteEntry(address=self.routerID,
                        metric=0, nexthop=self.routerID)
        self.localroute.init_timeout()
        self._routes.append(self.localroute)

        self.running()


    def running(self):
        print("RIP routing daemon now running on router %s..." % self.localroute.address)
        timeout = self.get_time_interval()
        trigger_timer = time.time()
        check_timer = self.get_time_interval(inval='check')
        while self.inputs:
            readable, writable, exceptional = select.select(self.inputs, self.outputs, self.inputs)

            if time.time() > timeout: # sending updates
                self.generate_update()
                timeout = self.get_time_interval()
                s = repr(self._routes)
                print((s[0:-1]+"\n"+s[-1]).replace(",",""))

            for s in readable: # receiving from the network
                rawdata = s.recv(512)
                data = RIPPacket(rawdata)
                self.process_response(data)

            if self.route_change == True and time.time() > trigger_timer: # triggered updates
                print("triggered update...")
                self.generate_update(triggered=True)
                trigger_timer = self.get_time_interval('trigger')

            if time.time() > check_timer: # timer to check for timeouts
                self.check_timeouts()
                check_timer = self.get_time_interval(inval='check')


    def generate_update(self,triggered=False):
        """
        Generates the RIP response message containing the routing table.
        Implements split horizon with poisoned reverse.

        Optional arg triggered:
        triggered is set to True if the update is
        a triggered update. In whichcase the changed flags are reset.
        """
        for iface in self.interfaces:
            header = Header(rID=self.routerID).serialize()
            msg = header
            count = 0
            for route in self._routes:
                if route.nexthop == iface[0]:
                    psnRte = deepcopy(route)  # create a copy to presevere original
                    psnRte.metric = RouteEntry.MAX_METRIC # posioning the metric
                    msg += psnRte.serialize()
                else:
                    msg += route.serialize()
                count += 1
                if count == self.MAX_ROUTES_PER_UPDATE:
                    self.outputs[0].sendto(msg, iface[1])
                    break

            if len(msg) > Header.SIZE:
                self.outputs[0].sendto(msg, iface[1])

        if triggered:
            self.route_change = False
            for routes in self._routes:
                routes.changed = False

    def get_time_interval(self, inval='update'):
        """
        Sets the various timers implemented by RIP
        optional argument inval:
        'update': default, the update timer
        'check': timer to check for timed out timers
        'trigger': trigger timer
        """
        if inval == 'update':
            offset = random.randrange(-self.TIMER_OFF_SET, self.TIMER_OFF_SET)
            interval = time.time() + self.update_timer + offset
        elif inval == 'check':
            interval = time.time() + random.randrange(1,4)
        elif inval == 'trigger':
            interval = time.time() + random.randrange(1,5)
        return interval

    def activate_ifaces(self, inputPorts, outputs):
        """
        activate the ports for the RIP daemon to listen on
        and send data through
        """
        self.inputs = []
        for port in inputPorts:
            # create UDP socket
            server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            server.setblocking(0)

            # bind the socket to the port
            server_address = (self.HOST, port)
            try:
                server.bind(server_address)
            except socket.error,e:
                sys.exit(e)

            # add it to the Routers inputs
            self.inputs.append(server)
        self.outputs = [self.inputs[0]]

        # ['output port', metric, router id]
        self.interfaces = []
        for output in outputs:
            iface = (self.HOST, output[0]) #("127.0.0.1, output port")
            self.interfaces.append([output[2],iface,output[1]]) # id, iface, metric


    def process_response(self, msg):
        """
        process each routing table entry received from the network
        Args:
        msg - the RIP entries
        """
        for rte in msg.rtes:
            rte.metric = min(rte.metric + self.lookup_metric(msg.header.rID), RouteEntry.MAX_METRIC)
            self.attempt_add_route(rte, msg.header.rID)

    def lookup_metric(self, rID):
        """
        check the metric to a peer router matching the arg rID - routerID
        """
        for iface in self.interfaces:
            if iface[0] == rID:
                return iface[2]

    def attempt_add_route(self, rte, rID):
        """
        Adds a new route to the routing table if the route has a metric less than 16
        and it is not already in the routing table. Updates a route if the new route
        is better than the old one. Marks routes for garbage collection if they have
        a metric of 16.
        """
        best_route = self.get_route(rte.address)
        rte.nexthop = rID
        if not best_route: # route is not in routing table
            if rte.metric == RouteEntry.MAX_METRIC:
                return
            self._routes.append(rte)
            rte.init_timeout()
            print("route to router %s added..." %rte.address)
        else:
            if rte.nexthop == best_route.nexthop:
                if best_route.metric != rte.metric:
                    if best_route.metric != RouteEntry.MAX_METRIC and \
                       rte.metric >= RouteEntry.MAX_METRIC:
                        self.init_garbage_collection_timer(best_route)
                    else:
                        self.update_route(rte)
                elif not best_route.marked_as_garbage:
                    best_route.init_timeout()
            elif rte.metric < best_route.metric:
                print("found a better route to %s via %s..." % \
                (rte.address, rte.nexthop))
                self.update_route(rte)

    def get_route(self, address):
        """
        gets a route from the routing table with
        a matching address
        """
        for route in self._routes:
            if route.address == address:
                return route
        return None

    def update_route(self,route):
        """
        updates a route in the routing table
        """
        addr = route.address
        for rte in self._routes:
            if rte.address == addr:
                self._routes[self._routes.index(rte)] = route
        route.init_timeout()
        print("route to %s updated" % route.address)

    def check_timeouts(self):
        """
        checks all of the timers in routing table entries to
        see if they're timed out.

        If an entry is timed out then it is considered stale and
        'marked as garbage'. If a garbage collection timer expires
        then the route is summarily removed from the routing table.
        """
        delta = datetime.timedelta(seconds=self.timeout_timer)
        cur_time = datetime.datetime.now()
        for rte in self._routes:
            if rte.address == self.routerID:
                continue
            if not rte.marked_as_garbage:
                elapsed = cur_time - rte.timeout
                if elapsed >= delta:
                    print("router %s timed out..." % rte.address)
                    self.init_garbage_collection_timer(rte)
                    continue
            else:
                delta = datetime.timedelta(seconds=self.garbage_collection_timer)
                cur_time = datetime.datetime.now()
                for rte in self._routes:
                    if rte.address == self.routerID:
                        continue
                    elapsed = cur_time - rte.timeout
                    if elapsed >= delta:
                        print("garbage collection timed out...")
                        self.garbage_collection(rte)

    def garbage_collection(self,route):
        """
        Removes garbage routes from the routing table
        """
        addr = route.address
        for rte in self._routes:
            if rte.address == addr:
                self._routes.pop(self._routes.index(rte))
                print("route to router %s removed..." % route.address)

    def init_garbage_collection_timer(self, route):
        """
        initialises the garbage collection timer and sets
        the marked_as_garbage flag
        """
        print("route to address %s marked as garbage..." % route.address)
        if not route.marked_as_garbage:
            route.init_timeout()
            self.route_change = True
            route.marked_as_garbage = True
            route.marked_as_changed = True
            route.metric = RouteEntry.MAX_METRIC



class RIPPacket(object):
    def __init__(self, data=None, header=None, rtes=None):
        """
        Creates a RIP packet from data received from the network
        or from an RIP header and list of routing table entries.
        """

        if data:
            self.init_from_port(data)
        elif header and rtes:
            self._init_from_host(header, rtes)
        else:
            raise(ValueError)

    def init_from_port(self, data):
        """Init from data received from the network."""
        datalength = len(data)

        numberOfRoutes = (datalength - Header.SIZE) / RouteEntry.SIZE
        self.header = Header(data[0:Header.SIZE])

        self.rtes = []
        routeStart = Header.SIZE
        routeEnd = Header.SIZE + RouteEntry.SIZE
        for i in range(numberOfRoutes):
            self.rtes.append(RouteEntry(rawdata=data[routeStart:routeEnd],rID=self.header.rID))
            routeStart += RouteEntry.SIZE
            routeEnd += RouteEntry.SIZE

    def _init_from_host(self, header, rtes):
        """Init using a header and rte list provided by the application."""
        if header.ver != 2:
            raise(ValueError("Only version 2 is supported."))
        self.header = header
        self.rtes = rtes

    def serialize(self):
        """Return a bytestring representing this packet in a form that
        can be transmitted across the network."""
        packed = self.header.serialize()
        for rte in self.rtes:
            packed += rte.serialize()
        return packed


class Header:
    FORMAT = ">BHH"
    SIZE = struct.calcsize(FORMAT)

    def __init__(self, rawdata=None, rID=None):
        """
        Create a RIP header eith from data received from the
        network or provided by the application. There are no
        request packets in this implementation so cmd will
        always be two. Likewise, version number will always
        be version two.
        """
        self.packed = None
        if rID:
            self.init_from_local(rID)
        elif rawdata:
            self.init_from_port(rawdata)
        else:
            raise(ValueError)

    def init_from_port(self, rawdata):
        """Init from data received from the network."""
        header = struct.unpack(self.FORMAT, rawdata)

        self.cmd = header[0]
        self.ver = header[1]
        self.rID = header[2]
        if self.rID < 0:
            raise(RIPException)

    def init_from_local(self, rID):
        """Init from data provided by the application."""
        self.rID = rID # routerID goes in place of the 16 bit all-zero field
        self.ver = 2 # alway version 2
        self.cmd = 2 # always type response

    def serialize(self):
        """
        Return a bytestring representing this packet header in a
        form that can be transmitted across the network.
        """
        return struct.pack(self.FORMAT, self.cmd, self.ver, self.rID)


class RouteEntry(object):
    FORMAT = ">HHIIII"
    SIZE = struct.calcsize(FORMAT)
    MIN_METRIC = 0
    MAX_METRIC = 16

    def __init__(self, rawdata=None, address=0, netmask=0, nexthop=0,
                 metric=0, tag=0, afi=2, rID=None):
        """
        Create a RIP Route Entry.
        """

        self.marked_as_garbage = False
        self.marked_as_changed = False

        self.afi = afi
        self.tag = tag
        self.netmask = netmask
        self.rID = rID
        self.init_timeout()

        if rawdata:
            self.init_from_port(rawdata)
        elif nexthop and \
             metric != None:
            self.init_from_local(address, nexthop, metric)
        else:
            raise(ValueError)

    def __repr__(self):
        return "\nRouteEntry: address = %s; nexthop = %s; metric = %d" \
        % (self.address, self.nexthop, self.metric)

    def init_from_local(self, address, nexthop, metric):
        """initialize from data provided by the application."""
        self.address = address
        self.nexthop = nexthop
        if self.nexthop == 0:
            self.nexthop = self.rID
        self.metric = metric


    def init_from_port(self, rawdata):
        """Initialize from data received through the port"""
        self.packed = None
        rte = struct.unpack(self.FORMAT, rawdata)
        self.address = rte[2]
        self.nexthop = rte[4]
        self.metric = rte[5]

        if self.nexthop == 0:
            self.nexthop = rID

        # Validation
        if not (self.MIN_METRIC <= self.metric <= self.MAX_METRIC):
            raise(FormatException)

    def init_timeout(self):
        """
        timeout used to keep track of last update and garbage collection
        """
        self.timeout = datetime.datetime.now()

    def serialize(self):
        """
        Convert into RIP packet entry format suitable to be sent over the
        network.
        """
        return struct.pack(self.FORMAT, self.afi, self.tag,
                                      self.address,
                                      self.netmask,
                                      self.nexthop,
                                      self.metric)


class RIPException(Exception):
    def __init__(self, msg=""):
        self.msg = msg



def readFromCommandline():
    if (len(sys.argv) == 1):
        sys.exit("\nExpected configuration file with .ini extension as argument.\n")
    elif (len(sys.argv) > 2):
        sys.exit("\nToo many arguments.\nExpected one configuration file with .ini\
            extension as argument.\n")
    else:
        configFile = sys.argv[1]
        if not os.path.isfile(configFile):
            sys.exit("\nArgument does not exist as a file.\n")
        else:
            fn, file_extension = os.path.splitext(configFile)
    if file_extension != '.ini':
        sys.exit("File must have .ini extension.")
    return printConfigDict(configFile)


def configSectionMap(config, section):
    """
    Args:
    config - the parsed config file
    section -
    """

    dict1 = {}
    try:
        for param in ['router-id','input-ports','outputs']:
            dict1[param] = ''.join(config.get(section,param).split()).split(',')
        dict1['timer'] = []
        if config.has_option(section, 'timer'):
            dict1['timer'] = ''.join(config.get(section,'timer').split()).split(',')
    except:
        sys.exit("\nConfiguration file formatting error.\n")
    return dict1

def coerceIntParams(configMap):
    int_params = ['router-id','input-ports','timer']
    for p in int_params:
        l = []
        for value in configMap[p]:
            try:
                value = int(value)
                if value > 0 and value < 64001:
                    l.append(value)
                else:
                    raise ValueError("router-id out of range")
            except ValueError,e:
                print(e)
                sys.exit("\nException on %s parameter in config file: %s \n" % (p,e))
        configMap[p] = l

    outputs = configMap['outputs']
    l = []
    for output in outputs:
        try:
            x,y,z = output.split('-')
            x = int(x)
            y = int(y)
            z = int(z)
            output = x,y,z
            if x < 0 or x > 64000:
                raise ValueError("Output-port out of range.")
            if y < 0:
                raise ValueError("Metric must be a postitive integer.")
            if z < 0 or z > 64000:
                raise ValueError("router-id out of range")
            l.append(output)
        except ValueError,e:
            print("\nException on %s parameter in config file. %s \n" % ('outputs',e))
            sys.exit(1)

    configMap['outputs'] = l
    return configMap

def printConfigDict(configFile):
    """
    Arg: configuration file name

    Returns tuple of dict configurationMap and bool includesTimers

    configurationMap maps parameter type to values
    includesTimers is False if timer is not a parameter, True otherwise
    """
    config = ConfigParser.ConfigParser()
    config.read(configFile)
    configurationMap = configSectionMap(config, "Configurations")
    configurationMap = coerceIntParams(configurationMap)
    return (configurationMap)


def main():
    """
    configDict:
    'router-id'
    'input-ports'
    'outputs' ['output port', metric, router id of peer router]
    'timer'
    """
    configDict = readFromCommandline()
    routerID = configDict['router-id']
    input_ports = configDict['input-ports']
    outputs = configDict['outputs']
    timer = configDict['timer']

    RIP_router = RIP(routerID, input_ports, outputs, timer)



if __name__ == "__main__":
    try:
      main()
    except KeyboardInterrupt:
        print("\nRouter shutdown...\nProgram aborted.")
